/* START TASK 1: Your code goes here */
let cells = document.getElementsByClassName('row');
for (let i = 0; i < cells.length; i++){
    cells[i].addEventListener('click', function(){
        if(this.classList.contains('first')){
            this.style.backgroundColor = 'blue';
            cells[i].classList.add('selected');
            let last = 2;
            if(!cells[i + 1].classList.contains('selected')){
                cells[i + 1].style.backgroundColor = 'blue';
                cells[i + 1].classList.add('selected');
            }
            if(!cells[i + last].classList.contains('selected')){
                cells[i + last].style.backgroundColor = 'blue';
                cells[i + last].classList.add('selected');
            }
        } else if(this.id === 'special'){
            for (let j = 0; j < cells.length; j++){
                if(!cells[j].classList.contains('selected')){
                    cells[j].style.backgroundColor = 'yellow';
                }
            }
        }else{
            this.classList.add('selected');
            this.style.backgroundColor = 'yellow';
        }
    });
}
/* END TASK 1 */

/* START TASK 2: Your code goes here */
function validate(){
    let inp = document.getElementById('inp');
    let inputValue = document.getElementById('inp').value;
    let text = document.createElement('div');
    if(/^\+380\d{9}$/.test(inputValue)){
        text.innerHTML = 'Data successfully sent';
        text.style.padding = '20px';
        text.style.backgroundColor = 'green';
        text.style.color = 'white';
        text.style.border = '2px solid black';
        text.style.margin = '10px';
    } else {
        text.innerHTML = 'Type number does not follow format +380*********';
        text.style.padding = '20px';
        text.style.backgroundColor = 'red';
        text.style.color = 'white';
        text.style.border = '2px solid black';
        text.style.margin = '10px';
        inp.style.borderColor = 'red';
    }
    document.getElementById('task2').appendChild(text);
    console.log(inputValue);
}
/* END TASK 2 */

/* START TASK 3: Your code goes here */
let ball = document.getElementById('ball');
let field = document.getElementById('field');
let aTeam = document.getElementById('aTeam');
let bTeam = document.getElementById('bTeam');
let aScore = 0, bScore = 0;
 
field.addEventListener('click', getClickPosition, false);
 
function getClickPosition(e) {
    let parentPosition = getPosition(e.currentTarget);
    let half = 2;
    let xPosition = e.clientX - parentPosition.x - ball.clientWidth / half;
    let yPosition = e.clientY - parentPosition.y - ball.clientHeight / half;
     
    ball.style.left = xPosition + 'px';
    ball.style.top = yPosition + 'px';
}
function getPosition(el) {
  let xPos = 0;
  let yPos = 0;
 
  while (el) {
    if (el.tagName === 'BODY') {
      let xScroll = el.scrollLeft || document.documentElement.scrollLeft;
      let yScroll = el.scrollTop || document.documentElement.scrollTop;
 
      xPos += el.offsetLeft - xScroll + el.clientLeft;
      yPos += el.offsetTop - yScroll + el.clientTop;
    } else {
      xPos += el.offsetLeft - el.scrollLeft + el.clientLeft;
      yPos += el.offsetTop - el.scrollTop + el.clientTop;
    }
 
    el = el.offsetParent;
  }
  return {
    x: xPos,
    y: yPos
  };
}

let aScoreArea = document.getElementById('aScoreArea');
let bScoreArea = document.getElementById('bScoreArea');

aScoreArea.addEventListener('click', function () {
    aScore++;
    aTeam.innerText = 'Team A: ' + aScore;
    let timer = 3000;
    let text = document.createElement('div');
        text.innerHTML = 'Team A score!';
        text.style.color = 'blue';
        text.style.margin = '10px';
        text.style.width = '700px';
        text.style.position = 'relative';
        text.style.top = '10px';
        text.style.left = '235px';
        document.getElementById('task3').appendChild(text);
        setTimeout(() => 
        document.getElementById('task3').removeChild(text), timer);
})
bScoreArea.addEventListener('click', function () {
    bScore++;
    bTeam.innerText = 'Team B: ' + bScore;
    let text = document.createElement('div');
    let timer = 3000;
        text.innerHTML = 'Team B score!';
        text.style.color = 'red';
        text.style.margin = '10px';
        text.style.width = '700px';
        text.style.position = 'relative';
        text.style.top = '10px';
        text.style.left = '235px';
        document.getElementById('task3').appendChild(text);
        setTimeout(() => 
        document.getElementById('task3').removeChild(text), timer);
})

/* END TASK 3 */
