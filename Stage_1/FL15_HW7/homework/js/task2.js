
function StartGame() {
    let startGame = confirm('Do you want to play a game?');
    let randomNumber;
    let prize = 100;
    let userMoney = 0;
    let guessedright = false;
    let max = 8;
    while (startGame) {
        randomNumber = Math.round(Math.random() * max);
        let tempPrize = prize;
        for (let i = 3; i > 0; i--) {
            let guess = Number(prompt(`Choose a roulette pocket number from 0 to ${max}\nAttempts left: ${i}
            \nTotal prize: ${userMoney}\nPossible prize on current attempt: ${tempPrize}`));
            if (randomNumber == guess) {
                guessedright = true;
                userMoney += tempPrize;
                max += 4;
                startGame = confirm(`Congratulation, you won!   Your prize is: ${userMoney} $. Do you want to continue?`);
                if (startGame == false) {
                    alert(`Thank you for your participation. Your prize is: ${userMoney} $`);
                    startGame = confirm('Do you want to play again?');
                }
                break;
            }
            tempPrize /= 2;
        }
        prize *= 2;
        if (guessedright == false) {
            alert(`Thank you for your participation. Your prize is: ${userMoney} $)`);
            startGame = confirm('Do you want to play again?');
            if (startGame == true) {
                prize = 100;
                max = 8;
                userMoney = 0;
            }
        }
        guessedright = false;
    }
    if (startGame == false) {
        alert('You did not become a billionaire, but can.');
    }

}
StartGame();
