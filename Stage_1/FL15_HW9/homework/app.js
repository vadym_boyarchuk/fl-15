function reverseNumber(num) {
    let rev = 0;
    if (num > 0) {
        while (num > 0) {
            let rem = num % 10;
            rev = rev * 10 + rem;
            num = Math.floor(num / 10);
        }
    }
    if (num < 0) {
        num *= -1;
        while (num > 0) {
            let rem = num % 10;
            rev = rev * 10 + rem;
            num = Math.floor(num / 10);
        }
        rev = '-' + rev;
        rev = parseInt(rev, 10);
    }
    console.log(rev);
    return rev;
}
reverseNumber(12345);
reverseNumber(-56789);
function forEach(arr, func) {
    for (let i = 0; i < arr.length; i++) {
        func(arr[i]);
    }
}
forEach([2, 5, 8], function (el) {
 console.log(el) 
});
function map(arr, func) {
    let array = [];
    forEach(arr, x => array.push(func(x)));
    return array;
}
console.log(map([2, 5, 8], function (el) {
 return el + 3; 
}));
console.log(map([1, 2, 3, 4, 5], function (el) {
 return el * 2; 
}));
function filter(arr, func) {
    let array = [];
    forEach(arr, x => {
        if (func(x) === true) {
            array.push(x);
        }
    });
    return array;
}
console.log(filter([2, 5, 1, 3, 8, 6], function (el) {
 return el > 3 
}));
console.log(filter([1, 4, 6, 7, 8, 10], function (el) {
 return el % 2 === 0 
}));
data = [
    {
        '_id': '5b5e3168c6bf40f2c1235cd6',
        'index': 0,
        'age': 39,
        'eyeColor': 'green',
        'name': 'Stein',
        'favoriteFruit': 'apple'
    },
    {
        '_id': '5b5e3168e328c0d72e4f27d8',
        'index': 1,
        'age': 38,
        'eyeColor': 'blue',
        'name': 'Cortez',
        'favoriteFruit': 'strawberry'
    },
    {
        '_id': '5b5e3168cc79132b631c666a',
        'index': 2,
        'age': 2,
        'eyeColor': 'blue',
        'name': 'Suzette',
        'favoriteFruit': 'apple'
    },
    {
        '_id': '5b5e31682093adcc6cd0dde5',
        'index': 3,
        'age': 17,
        'eyeColor': 'green',
        'name': 'Weiss',
        'favoriteFruit': 'banana'
    }
]
function getAdultAppleLovers(data) {
    let tempData = filter(data, function (el) {
 return el.age > 18 && el.favoriteFruit === 'apple' 
});
    let Name = map(tempData, x => x.name);
    return Name;
}
console.log(getAdultAppleLovers(data));
function getKeys(obj) {
    let allkey = obj;
    let keys = [];
    for (let key in allkey) {
        keys.push(key);
    }
    return keys;
}
console.log(getKeys({ keyOne: 1, keyTwo: 2, keyThree: 3 }));
function getValues(obj) {
    let allkey = obj;
    let keys = [];
    for (let key in allkey) {
        keys.push(allkey[key]);
    }
    return keys;
}
console.log(getValues({ keyOne: 1, keyTwo: 2, keyThree: 3 }));
function showFormattedDate(dateObj) {
    const d = dateObj;
    const ye = new Intl.DateTimeFormat('en', { year: 'numeric' }).format(d);
    const mo = new Intl.DateTimeFormat('en', { month: 'short' }).format(d);
    const da = new Intl.DateTimeFormat('en', { day: '2-digit' }).format(d);
    return `It is ${da} of ${mo}, ${ye}`;
}
console.log(showFormattedDate(new Date('2018-08-27T01:10:00')));