const birthday22 = new Date(2000, 9, 22);
const birthday23 = new Date(2000, 9, 23);
function getAge(date) {
    let today = new Date(2020, 9, 22);
    let datenow = new Date(today.getFullYear(), date.getMonth(), date.getDate());
    let age;

    age = today.getFullYear() - date.getFullYear();
    if (today < datenow) {
        age = age - 1;
    }
    return age;

}
console.log(getAge(birthday22));
console.log(getAge(birthday23));

function getWeekDay(date) {
    date = new Date(date);
    let days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    return days[date.getDay()];
}
console.log(getWeekDay(Date.now()));
console.log(getWeekDay(new Date(2020, 9, 22)));

function getAmountDaysToNewYear() {
    // let today = new Date('August 30, 2020 00:00:00');
    // let today = new Date('January 1, 2020 00:00:00');
    let today = new Date();
    let newYear = new Date(today.getFullYear(), 11, 31);
    if (today.getMonth() == 11 && today.getDate() > 31) {
        newYear.setFullYear(newYear.getFullYear() + 1);
    }
    let one_day = 1000 * 60 * 60 * 24;

    resualt = Math.ceil((newYear.getTime() - today.getTime()) / (one_day));
    return resualt;
}
console.log(getAmountDaysToNewYear());

function getProgrammersDay(year) {
    let isLeefYeer = (year % 4 === 0)
        ? ((year % 100 === 0) ? ((year < 1918 || (year % 400 === 0)) ? true : false) : true)
        : false;

    let dayNumber = (year === 1918 ? 26 : (isLeefYeer ? 12 : 13));
    let day = `${year}.09.${dayNumber}`;
    let resualt = getWeekDay(day);
    return `${dayNumber} Sep,${year} (${resualt})`;
}
console.log(getProgrammersDay(2020));
console.log(getProgrammersDay(2019));

function howFarIs(string) {
    let today = new Date(2020, 9, 22);
    let day = today.getDay();
    let weekday = ['Sunday',
        'Monday',
        'Tuesday',
        'Wednesday',
        'Thursday',
        'Friday',
        'Saturday'][day];
    let objDay = {
        1: 'Monday',
        2: 'Tuesday',
        3: 'Wednesday',
        4: 'Thursday',
        5: 'Friday',
        6: 'Saturday',
        7: 'Sunday'
    }
    let numberDay;
    let str = string;
    let count;
    for (let key in objDay) {
        if (objDay[key].toLocaleLowerCase() === string.toLocaleLowerCase()) {
            numberDay = key;
            str = objDay[key];
        }
    }
    if (day > numberDay) {
        count = (parseInt(day) + parseInt(numberDay)) - 1;
    } else {
        count = day - numberDay;
    }
    if (count < 0) {
        count *= -1;
    }
    if (weekday.toLocaleLowerCase() === string.toLocaleLowerCase()) {
        return `Hey, today is ${weekday})`;
    }
    return `It's ${count} day(s) left till ${str}.`;

}
console.log(howFarIs('friday'));
console.log(howFarIs('Thursday'));


function isValidIdentifier(string) {

    let regex = /^([a-zA-Z][a-zA-Z_$]*)$/
    return regex.test(string);
}
console.log(isValidIdentifier('myVar!')); // false
console.log(isValidIdentifier('myVar$')); // true
console.log(isValidIdentifier('myVar_1')); // true
console.log(isValidIdentifier('1_myVar')); // false

function capitalize(string) {

    let re = /(\b[a-z](?!\s))/g;
    string = string.replace(re, function (x) { return x.toUpperCase();});
    return string

}
const testStr = "My name is John Smith. I am 27.";
console.log(capitalize(testStr)); // "My Name Is John Smith. I Am 27."



function isValidAudioFile(string){
    return /\.(mp3|flac|alac|aac)$/i.test(string);

}
console.log(isValidAudioFile('file.mp4')); // false
console.log(isValidAudioFile('my_file.mp3')); // false
console.log(isValidAudioFile('file.mp3')); // true
