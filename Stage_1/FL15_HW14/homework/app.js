
const appRoot = document.getElementById('app-root');
appRoot.insertAdjacentHTML('afterend', `
<div class="search">
<div>
    <h2>Countries Search</h2>
</div>
<div class="search-item">
    <div>
        <p>Please choose type of search</p>
    </div>
    <div>
        <input name="r" id="byRegion" value="region" type="radio">
        <label>By Region</label>
        <br>
        <input name="r" id="byLanguage" value="language" type="radio" />
        <label>By Language</label>
    </div>
</div>
<div >
    <label>Please choose search query :
        <select id="select" disabled name="countryList" >
            <option value="none" hidden="">Select value</option>
        </select>
    </label>
    <div id = "text">
     </div>
</div>

</div>
<div class="tableCountry">
</div>
`)
document.addEventListener('DOMContentLoaded', () => {
    'use strict';
    let select = document.getElementById('select');
    select.setAttribute('disabled', false);

    if (!select.getAttribute('disabled')) {
    } else {
        console.log('ffff');
        const p = document.getElementById('text');
        p.insertAdjacentHTML('afterbegin', '<p>No items, please choose search query</p>');
    }
    function checkRegion() {
        select.removeAttribute('disabled');
        let inp = document.getElementsByName('r');
        let arrRegion = externalService.getRegionsList();
        let arrLanguage = externalService.getLanguagesList();
        for (let i = 0; i < inp.length; i++) {
            if (inp[i].type === 'radio' && inp[i].checked) {
                console.log('selected: ' + inp[i].value);
                if (inp[i].value === 'region') {
                    let select = document.getElementById('select');
                    let length = select.options.length;
                    for (let i = length - 1; i >= 1; i--) {
                        select.options[i] = null;
                    }
                    for (let i = 0; i < arrRegion.length; i++) {
                        let opt = arrRegion[i];
                        let el = document.createElement('option');
                        el.textContent = opt;
                        el.value = opt;
                        select.appendChild(el);
                    }
                }
                if (inp[i].value === 'language') {
                    let select = document.getElementById('select');
                    let length = select.options.length;
                    for (let i = length - 1; i >= 1; i--) {
                        select.options[i] = null;
                    }
                    for (let i = 0; i < arrLanguage.length; i++) {
                        let opt = arrLanguage[i];
                        let el = document.createElement('option');
                        el.textContent = opt;
                        el.value = opt;
                        select.appendChild(el);
                    }
                }
            }
        }

        console.log(arrRegion);

    }
    document.getElementById('byRegion').addEventListener('change', checkRegion);
    document.getElementById('byLanguage').addEventListener('change', checkRegion);

    function createTable() {

        document.querySelector('.tableCountry').innerHTML = `<table id="myTable" 
        class="table-sortable table country" ></table>`
        let select = document.getElementById('select');
        let language = select.value;
        let inp = document.getElementsByName('r');
        for (let i = 0; i < inp.length; i++) {
            if (inp[i].value === 'region') {
                document.getElementById('text').style.display = 'none';
                var region = externalService.getCountryListByRegion(language);

            }
            if (inp[i].value === 'language') {
                var countries = externalService.getCountryListByLanguage(language);
                document.getElementById('text').style.display = 'none';
            }

        }
        let mapper = {
            'Country Name': 'name',
            'Capital': 'capital',
            'Word Region': 'region',
            'Languages': 'languages',
            'Area': 'area',
            'Flag': 'flagURL'
        }
        for (let key in mapper) {
            let row = document.createElement('tr');
            row.id = key;
            row.innerHTML = `<th colspan="2">${key}</th>`;
            document.querySelector('.country').appendChild(row);
        }
        for (let country of region) {
            for (let key in mapper) {
                let row = document.getElementById(key);
                let child = document.createElement('td');
                let mapperKey = mapper[key];
                let countryProp = country[mapperKey];
                /* if(key==='Flag'){
                    debugger;
                    child.innerHTML = `<img src="${countryProp}"/>`
                    row.appendChild(child);
                } */
                if (typeof countryProp === 'object') {
                    for (let k in countryProp) {
                        child.innerText = countryProp[k];
                        row.appendChild(child);
                    }
                } else {
                    child.innerText = countryProp;
                    row.appendChild(child);
                }
            }
        }
        for (let country of countries) {
            for (let key in mapper) {
                let row = document.getElementById(key);
                let child = document.createElement('td');
                let mapperKey = mapper[key];
                let countryProp = country[mapperKey];
                /* if(key==='Flag'){
                    debugger;
                    child.innerHTML = `<img src="${countryProp}"/>`
                    row.appendChild(child);
                } */
                if (typeof countryProp === 'object') {
                    for (let k in countryProp) {
                        child.innerText = countryProp[k];
                        row.appendChild(child);
                    }
                } else {
                    child.innerText = countryProp;
                    row.appendChild(child);
                }
            }
        }
    }


    document.getElementById('select').addEventListener('change', createTable);


    function sortTableByColumn(table, column, asc = true) {
        const dirModifier = asc ? 1 : -1;
        const tBody = table.tBodies[0];
        const rows = Array.from(tBody.querySelectorAll('tr'));

        // Sort each row
        const sortedRows = rows.sort((a, b) => {
            const aColText = a.querySelector(`td:nth-child(${column + 1})`).textContent.trim();
            const bColText = b.querySelector(`td:nth-child(${column + 1})`).textContent.trim();

            return aColText > bColText ? 1 * dirModifier : -1 * dirModifier;
        });

        // Remove all existing TRs from the table
        while (tBody.firstChild) {
            tBody.removeChild(tBody.firstChild);
        }

        // Re-add the newly sorted rows
        tBody.append(...sortedRows);

        // Remember how the column is currently sorted
        table.querySelectorAll('th').forEach(th => th.classList.remove('th-sort-asc', 'th-sort-desc'));
        table.querySelector(`th:nth-child(${column + 1})`).classList.toggle('th-sort-asc', asc);
        table.querySelector(`th:nth-child(${column + 1})`).classList.toggle('th-sort-desc', !asc);
    }

    document.querySelectorAll('.table-sortable th').forEach(headerCell => {
        headerCell.addEventListener('click', () => {
            const tableElement = headerCell.parentElement.parentElement.parentElement;
            const headerIndex = Array.prototype.indexOf.call(headerCell.parentElement.children, headerCell);
            const currentIsAscending = headerCell.classList.contains('th-sort-asc');

            sortTableByColumn(tableElement, headerIndex, !currentIsAscending);
        });
    });
        let th = document.getElementsByTagName('th');
        console.log(th);
        debugger;
        for (let c = 0; c < th.length; c++) {
            debugger;
            th[c].forEach.call(function(el){
                el.addEventListener('click',() => {

                    function item(c) {

                        return function () {
                            console.log(c)
                            sortTable(c)
                        }
                    }
                    item(c);
                    function sortTable(c) {
            
                        let table, rows, switching, i, x, y, shouldSwitch;
                        table = document.getElementById('myTable');
                        switching = true;
                        /*Make a loop that will continue until
                        no switching has been done:*/
                        while (switching) {
                            //start by saying: no switching is done:
                            switching = false;
                            rows = table.rows;
                            /*Loop through all table rows (except the
                            first, which contains table headers):*/
                            for (i = 1; i < rows.length - 1; i++) {
                                //start by saying there should be no switching:
                                shouldSwitch = false;
                                /*Get the two elements you want to compare,
                                one from current row and one from the next:*/
                                x = rows[i].getElementsByTagName('TD')[c];
                                y = rows[i + 1].getElementsByTagName('TD')[c];
                                //check if the two rows should switch place:
                                if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
                                    //if so, mark as a switch and break the loop:
                                    shouldSwitch = true;
                                    break;
                                }
                            }
                            if (shouldSwitch) {
                                /*If a switch has been marked, make the switch
                                and mark that a switch has been done:*/
                                rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
                                switching = true;
                            }
                        }
                    }
                
                })
            });
        }
    //document.getElementById('Country Name').addEventListener("click", sortCountry);

})
