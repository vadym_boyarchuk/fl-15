

function isEquals(a, b) {
    return a === b;
}
console.log(isEquals(3, 3));

function isBigger(a, b) {
    return a > b;
}
console.log(isBigger(5, -1));

function storeNames(...string) {
    let arr = [];
    for (let i = 0; i < string.length; i++) {
        arr[i] = string[i];
    }
    return arr;
}
console.log(storeNames('Tommy Shelby', 'Ragnar Lodbrok', 'Tom Hardy'));

function getDifference(a, b) {

    if (a > b) {
        return a - b;
    } else {
        return b - a;
    }

}
console.log(getDifference(5, 3));
console.log(getDifference(5, 8));

function negativeCount(arr) {
    let array = [];
    let count = 0;
    for (let i = 0; i < arr.length; i++) {
        array[i] = arr[i];
        if (array[i] < 0) {
            count++;
        }
    }
    return count;
}
console.log(negativeCount([4, 3, 2, 9]));
console.log(negativeCount([0, -3, 5, 7]));

function letterCount(string, string1) {

    let count = 0;
    for (let i = 0; i < string.length; i++) {

        if (string[i].includes(string1)) {
            count++;
        }

    }
    return count;
}
console.log(letterCount('Marry', 'r'));
console.log(letterCount('Barny', 'y'));
console.log(letterCount('', 'z'));

function countPoints(arr) {
    let count = 0;
    let array;
    for (let i = 0; i < arr.length; i++) {
        array = arr[i].split(':');
        if (parseInt(array[0]) > parseInt(array[1])) {
            count += 3;
        }
        if (array[0] === array[1]) {
            count++;
        }

    }
    return count;
}
console.log(countPoints(['100:90', '110:98', '100:100', '95:46', '54:90', '99:44', '90:90', '111:100']));

