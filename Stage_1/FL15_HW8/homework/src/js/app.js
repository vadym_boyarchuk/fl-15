document.addEventListener('DOMContentLoaded', () => {
    'use strict';
    let eventName = prompt('Input event name', 'meeting');
    let time = document.getElementById('time');
    let name = document.getElementById('name');
    let place = document.getElementById('place');
    let regex = /^(2[0-3]|[01]?[0-9]):([0-5]?[0-9])$/;
    const form = document.querySelector('form');
    let errors = true;
    form.addEventListener('submit', (even) => {
        even.preventDefault();
        for (let elem of form.elements) {
            if (elem.classList.contains('main-input')) {
                if (elem.value === '') {
                    alert('Input all data')
                    errors = false;
                }else{
                    errors = true;
                }  
            }
        }
        if (!regex.test(time.value)) {
            alert('Enter time in format hh:mm');
            errors = false;
        }
        if (errors === true) {
            console.log(`${name.value} has a ${eventName} today at ${time.value} somewhere in ${place.value}`);
        }
    });
    document.getElementById('converter').onclick = () => {
        function convert() {

            let euro = parseFloat(prompt('Input amount of euro', ''), 10).toFixed(2);
            let dollar = parseFloat(prompt('Input amount of dollar', ''), 10).toFixed(2);
            if (euro < 0 || dollar < 0) {
                let conf = confirm('Input positive value, try again press Yes, exit NO');
                if (conf) {
                    convert();
                }
            }
            let newEuro = parseFloat(euro * 30.20, 10);
            let newDollar = parseFloat(dollar * 27.58, 10);
            alert(`${euro}euros are equal${newEuro.toFixed(4)} hrns, ${dollar} are equal ${newDollar.toFixed(4)}hrns`);
        }
        convert();
    };
});