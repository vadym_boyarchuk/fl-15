function isFunction(functionToCheck) {
    return functionToCheck && {}.toString.call(functionToCheck) === '[object Function]';
}

const pipe = (value, ...funcs) => {
    let res = value;
    for(let i = 0; i < funcs.length; i++){
        if(isFunction(funcs[i])){
            res = funcs[i](res);
        } else{
            throw new Error('Parameter that has been passed is not a function!');
        }
    }
    return res;
};