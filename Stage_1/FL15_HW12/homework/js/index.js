function visitLink(path) {
    if(localStorage.getItem('counter1') === undefined){
        localStorage.setItem('counter1', 0);
        localStorage.setItem('counter2', 0);
        localStorage.setItem('counter3', 0);
    }
    if (path === 'Page1'){
        let a = localStorage.getItem('counter1');
        localStorage.setItem('counter1', ++a);
    }
    if (path === 'Page2'){
        let b = localStorage.getItem('counter2');
        localStorage.setItem('counter2', ++b);
    }
    if (path === 'Page3'){
        let c = localStorage.getItem('counter3');
        localStorage.setItem('counter3', ++c);
    }
}

function viewResults() {
    let countOfItems = 3;
    let ul = document.createElement('ul');
    document.getElementsByClassName('container')[0].appendChild(ul);
    for(let i = 1; i <= countOfItems; i++){
            let li = document.createElement('li');
            ul.appendChild(li);
            li.innerHTML += `You visited Page${i} ${localStorage.getItem(`counter${i}`)} time(s)`;
    }
    for(let i = 1; i <= countOfItems; i++){
        localStorage.setItem(`counter${i}`, 0);
    }
}