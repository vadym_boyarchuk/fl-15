
function getUsers() {
    return new Promise((resolve, reject) => {
        fetch('https://jsonplaceholder.typicode.com/users')
            .then(response => response.json())
            .then(users => resolve(users))
            .catch(err => reject(err));
    })
}


function editUsers(id, name, username, email, phone, website) {
    let obj = {
        id: id,
        name: name,
        username: username,
        email: email,
        phone: phone,
        website: website
    };
    let div = document.getElementById('spinner');
    div.removeAttribute('hidden');
    fetch(`https://jsonplaceholder.typicode.com/users/${id}`, {
        method: 'PUT',
        body: JSON.stringify(obj),
        headers: {
            'Content-type': 'application/json'
        }
    })
        .then(response => {
            if (!response.ok) {
                throw new Error(`Error code : ${response.status}`)
            }
            div.setAttribute('hidden', '');
            return response.json();
        })
        .then((data) => {
            console.log('Edited user')
            console.log(data);

            return `id :${data.id} , name  ${data.name}, userName ${data.userName}`;
        }).catch((err) => {
            console.log('Fetch error: ', err);
        });
    console.log(obj);
    return obj
}

function deleteUsers(id) {
    let div = document.getElementById('spinner');
    div.removeAttribute('hidden');
    fetch(`https://jsonplaceholder.typicode.com/users/${id}`, {
        method: 'DELETE'
    })
        .then(response => {
            if (!response.ok) {
                throw new Error(`Error code : ${response.status}`)
            }
            div.setAttribute('hidden', '');
            return response.json()
        }).then((data) => {

            console.log('User deleted')
            console.log(data);
        }).catch((err) => {
            console.log('Fetch error: ', err);
        });
}


let arrUsers = [];
getUsers().then(users => arrUsers.push(users));
console.log('arrr');
console.log(arrUsers);
let btnGet = document.querySelector('#btnCreateTable');
let myTable = document.querySelector('#table');
let btnEdit = document.querySelector('#editUser');
let btnDelete = document.querySelector('#deleteUser');
let headers = ['Id', 'Nane', 'UserName', 'Email', 'Phone', 'Website'];


btnDelete.addEventListener('click', () => {
    let id = document.getElementById('Del').value;
    deleteUsers(id);
    document.getElementById('result1').value = 'Deleted';
})


btnEdit.addEventListener('click', () => {
    let id = document.getElementById('id').value;
    let name = document.getElementById('name').value;
    let userName = document.getElementById('username').value;
    let email = document.getElementById('email').value;
    let phone = document.getElementById('phone').value;
    let website = document.getElementById('website').value;
    editUsers(id, name, userName, email, phone, website);
    document.getElementById('result').value = 'Edited';
})

function getPostComment(id) {
    const [, userId] = id.split('-');
    let div = document.getElementById('spinner');
    div.removeAttribute('hidden');
    const data = Promise.all([
        fetch(`https://jsonplaceholder.typicode.com/posts?userId=${userId}`).then(response => {
            if (!response.ok) {
                throw new Error(`Error code : ${response.status}`)
            }

            div.setAttribute('hidden', '');
            return response.json('');
        })
            .then((data) => {
                console.log('POSTS')
                console.log(data);
                div.removeAttribute('hidden');
                return data;
            }).catch((err) => {
                console.log('Fetch error: ', err);
            }),
        fetch(`https://jsonplaceholder.typicode.com/comments?postId=${userId}`).then(response => {
            if (!response.ok) {
                throw new Error(`Error code : ${response.status}`)
            }
            div.setAttribute('hidden', '');
            window.location.href = `https://jsonplaceholder.typicode.com/comments?postId=${userId}`;
            return response.json();
        })
            .then((data) => {
                console.log('Comment')
                console.log(data);
                return data;
            }).catch((err) => {
                console.log('Fetch error: ', err);
            })
    ]);
    console.log('data');
    console.log(data);
}
btnGet.addEventListener('click', () => {
    let table = document.createElement('table');
    let headerRow = document.createElement('tr');

    headers.forEach(headerText => {
        let header = document.createElement('th');
        let textNode = document.createTextNode(headerText);
        header.appendChild(textNode);
        headerRow.appendChild(header);
    });

    table.appendChild(headerRow);

    arrUsers.forEach(emp => {
        emp.forEach(el => {
            let row = document.createElement('tr');
            Object.values(el).forEach(text => {
                let cell = document.createElement('td');
                let textNode = document.createTextNode(text);
                let btn = document.createElement('button');
                btn.setAttribute('id', 'N-' + el.id);
                btn.onclick = function () {
                    myFunc(btn.id);
                };
                btn.appendChild(cell);
                cell.appendChild(textNode);
                row.appendChild(cell);
                row.appendChild(btn);
            })
            table.appendChild(row);
        })

    });

    myTable.appendChild(table);

});

function myFunc(id) {
    getPostComment(id);
}




