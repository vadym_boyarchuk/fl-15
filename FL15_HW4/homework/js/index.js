class Deck {
    constructor() {
        this.cards = [];
        this.count;
    }

    get count() {
        return this._count;
    }
    shuffle() {
        const { cards } = this;
        let m = cards.length, i;

        while (m) {
            i = Math.floor(Math.random() * m--);
            [cards[m], cards[i]] = [cards[i], cards[m]];
        }

        return this;
    }
    draw(n) {
        let poped = [];
        for (let i = 0; i < n; i++) {
            poped = this.cards.pop();
        }
        return poped;
    }
}
class Card {
    constructor() {
        this.suit = ['Hearts', 'Spades', 'Clubs', 'Diamonds'];
        this.rank = ['Ace', 2, 3, 4, 5, 6, 7, 8, 9, 10, 'Jack', 'Queen', 'King'];
        this.isFaceCard;
    }
    get isFaceCard() {
        if (this.rank > 1 || this.rank < 10) {
            return this.isFaceCard;
        } else {
            return this._isFaceCard;
        }
    }
    toString() {
        const deck = new Deck();
        const suits = this.suit;
        const values = this.rank;
        for (let suit in suits) {
            for (let value in values) {
                deck.cards.push(`${values[value]} of ${suits[suit]}`);
            }
        }
        return deck.cards;
    }
    compare(cardOne, cardTwo) {

        if (cardOne[0] === 'K' && cardTwo[0] === 'Q') {
            return cardOne
        }
        if (cardOne[0] === 'Q' && cardTwo[0] === 'J') {
            return cardOne;
        }
        if (cardOne[0] === 'J' && cardTwo[0] === 'Q') {
            return cardOne;
        }
        if (cardTwo[0] === 'K' && cardOne[0] === 'Q') {
            return cardTwo
        }
        if (cardTwo[0] === 'Q' && cardOne[0] === 'J') {
            return cardTwo;
        }
        if (cardTwo[0] === 'J' && cardOne[0] === 'Q') {
            return cardTwo;
        } else
            if (cardOne[0] > cardTwo[0]) {
                return cardOne;
            } else {
                return cardTwo;
            }
    }
}
class Player {
    constructor() {
        this.name;
        this.wins;
        this.deck;
    }
    play(playerOne, playerTwo) {
        let players = [];
        const pl1 = new Player();
        pl1.name = playerOne;
        const pl2 = new Player();
        pl2.name = playerTwo;
        players.push(pl1);
        players.push(pl2);
        const card = new Card();
        const deck1 = new Deck();
        const deck2 = new Deck();
        const allCards = card.toString();
        deck1.cards = allCards;
        deck2.cards = allCards;
        deck1.shuffle();
        deck2.shuffle();
        deck1.cards = deck1.cards.slice(0, 26);
        deck2.cards = deck2.cards.slice(26, 52);

        let point1 = 0;
        let point2 = 0;
        let cardCompare;
        for (let i = 0; i < 26; i++) {
            let card1 = deck1.draw(1);
            let card2 = deck2.draw(1);
            cardCompare = card.compare(card1, card2);
            if (cardCompare === card1) {
                point1 += 1;
            } else {
                point2 += 1;
            }
        }
        pl1.wins = point1;
        pl2.wins = point2;
        if (point1 > point2) {
            return `${pl1.name} wins ${point1} to ${point2}`;
        } else {
            return `${pl2.name} wins ${point2} to ${point1}`;
        }
    }
}
let play1 = new Player();
console.log(play1.play('Bob', 'BiB'));

class Employee {

    constructor() {
        this.id;
        this.firstName;
        this.lastName;
        this.birthday;
        this.salary;
        this.position;
        this.department;
        this.age;
        this.fullName;
        if (!Employee._EMPLOYEES) {
            Employee._EMPLOYEES = [];
        }
        Employee._EMPLOYEES.push(this);
    }

    static get EMPLOYEES() {
        return Employee._EMPLOYEES;
    }
    get age() {
        return new Date().getFullYear() - this.birthday;
    }
    get fullName() {
        let full = `${this.firstName} ${this.lastName}`
        return full;
    }
    quit() {
        console.log(Employee._EMPLOYEES);
        for (let i in Employee._EMPLOYEES) {
            if (Employee._EMPLOYEES[i].id === this.id) {
                Employee._EMPLOYEES.splice(i, 1);
            }
        }
        return Employee._EMPLOYEES;
    }
    retire() {
        console.log('It was such a pleasure to work with you!');
        for (let i in Employee._EMPLOYEES) {
            if (Employee._EMPLOYEES[i].id === this.id) {
                Employee._EMPLOYEES.splice(i, 1);
            }
        }
        return Employee._EMPLOYEES;

    }
    getFired() {
        console.log('Not a big deal!')
        for (let i in Employee._EMPLOYEES) {
            if (Employee._EMPLOYEES[i].id === this.id) {
                Employee._EMPLOYEES.splice(i, 1);
            }
        }
        return Employee._EMPLOYEES;
    }
    changeDepartment(newDepartment) {
        this.department = newDepartment;
    }
    changePosition(newPosition) {
        this.newPosition = newPosition;
    }
    changeSalary(newSalary) {
        this.salary = newSalary;
    }
    getPromoted(benefits) {
        if (benefits === 'salary') {
            this.salary = this.changeSalary(2000);
        }
        if (benefits === 'department') {
            this.department = this.changeDepartment('fffff');
        }
        if (benefits === 'position') {
            this.position = this.changePosition('managed');
        }
        console.log('Yoohooo!')
    }
    getDemoted(punishment) {
        if (punishment === 'salary') {
            this.salary = this.changeSalary(1500);
        }
        if (punishment === 'department') {
            this.department = this.changeDepartment('undepartment');
        }
        if (punishment === 'position') {
            this.position = this.changePosition('un managed');
        }
        console.log('Damn!')
    }
}

const emp = new Employee();
emp.id = 1;
emp.firstName = 'Vasyan';
emp.lastName = 'Bob';
emp.birthday = 2002;
emp.salary = 1000;
emp.position = 'Manager';
emp.department = 'llll';
const emp1 = new Employee();
emp1.id = 2;
emp1.firstName = 'BIB';
emp1.lastName = 'BIB';
emp1.birthday = 2005;
emp1.salary = 5000;
emp1.position = 'Manager';
emp1.department = 'llll';
emp.EMPLOYEES = emp;
emp.EMPLOYEES = emp1;
emp.getPromoted('salary')
//emp.quit();
emp1.quit();
// console.log(emp.EMPLOYEES);
// console.log(emp1.EMPLOYEES);

class Manager extends Employee {

    constructor(...args) {
        super(...args);
        this.managedEmployees;
    }
    get managedEmployees() {
        console.log(Employee._EMPLOYEES);
        let tmp=[];
        for (let i in Employee._EMPLOYEES) {
            if (Employee._EMPLOYEES[i].department !== 'manager') {
                tmp = Employee._EMPLOYEES[i];
            }
        }
        this._managedEmployees = tmp;
        return this._managedEmployees;

    }
}
class BlueCollarWorker extends Employee {

}
class HRManager extends Manager {
    constructor() {
        let department = 'hr';
        super(department);
    }
}
class SalesManager extends Manager {
    constructor() {
        let department = 'sales';
        super(department);
    }
}
function mangerPro() {
    const emp = new Employee();
    emp.id = 1;
    emp.firstName = 'Vasyan';
    emp.lastName = 'Bob';
    emp.birthday = 2002;
    emp.salary = 1000;
    emp.position = 'aaaaa';
    emp.department = 'llll';
    const manager = new Manager();
    return `All implois ${manager.managedEmployees}`;
}
mangerPro();