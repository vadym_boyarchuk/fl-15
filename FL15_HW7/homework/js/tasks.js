function maxElement(arr) {
    return Math.max(...arr);
}
const array = [1, 13, 33, 15, 3];
console.log(maxElement(array));

function copyArray(arr) {

    let array = [...arr];
    return array;
}
const arr = [1, 2, 3];
const copiedArray = copyArray(arr);
console.log(arr, copiedArray);
console.log(arr === copiedArray);

function addUniqueId(obj) {
    let obj1 = Object.assign(obj);
    return {
        'unique identifier': Symbol(),
        name: obj1.name
    }
}
console.log(addUniqueId({ name: 123 }));

function regroupObject(obj) {
    let obj1 = Object.assign({}, obj);
    let obj2;
    let obj3 = {};
    let tmp;
    for (let i in obj1) {
        if (typeof obj1[i] === 'object') {
            obj2 = {
                user: obj1[i]
            }
            obj3 = {
                university: obj1[i].university
            }
        } else {
            tmp = obj1[i];
        }
    }
    for (let i in obj2) {
        delete obj2[i].university;
        obj2[i] = {
            id: obj2[i].id,
            age: obj2[i].age,
            firstName: tmp
        }
    }
    let test = Object.assign(obj3, obj2);
    for (let i in test) {
        if (test[i] === 'university') {
            obj3 = {
                university: test[i]
            }
        }
    }
    return test;
}
const oldObj = { name: 'Someone', details: { id: 1, age: 11, university: 'UNI' } };

console.log(regroupObject(oldObj));


function findUniqueElements(arr) {
    let tmpArr = [];
    let mySet = new Set();
    for (let i = 0; i < arr.length; i++) {
        mySet.add(arr[i]);
    }
    for (let it of mySet) {
        tmpArr.push(it);
    }
    return tmpArr;
}
const test = [1, 1, 1, 2, 2, 3, 3, 4];
console.log(findUniqueElements(test));

function hideNumber(num) {
    let length = num.length - 4;
    let str = num.substr(length);
    return str.padStart(length, '*');
}

const number = '09876543456789';
console.log(hideNumber(number));
const required = (msg) => {
    throw new Error(msg)
}
const add = (a = required('Missing property'), b = required('Missing property')) => {
    return console.log(a + b);
}
add(1, 3);
//add(1);

function getPeople() {
    return fetch(`https://jsonplaceholder.typicode.com/users`)
        .then(response => {
            if (!response.ok) {
                throw new Error(`Error code : ${response.status}`)
            }
            return response.json();
        })
        .then((data) => {
            console.log(data);
        }).catch((err) => {
            console.log('Fetch error: ', err);
        });
}
getPeople();

async function getPeopleAsync() {
    try {
        const response = await fetch('https://jsonplaceholder.typicode.com/users/');
        if (!response.ok) {
            throw new Error(`Failed with status code: ${response.status}`);
        }
        const data = await response.json()
        return console.log(data);
    } catch (err) {
        console.log('Error :', err);
    }
}
getPeopleAsync();
