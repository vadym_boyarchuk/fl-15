const gulp = require('gulp'),
  imagemin = require('gulp-imagemin'),
  sass = require('gulp-sass'),
  rename = require('gulp-rename'),
  concat = require('gulp-concat'),
  babel = require('gulp-babel'),
  minify = require('gulp-minify'),
  browserSync = require('browser-sync');

function htmlInit(cb) {
  gulp.src('./index.html').pipe(
    gulp.dest('../dist/')
  ).pipe(
    browserSync.stream()
  );
  cb();
}

function imagesInit(cb) {
  gulp.src('./img/*.jpg', { allowEmpty: true })
    .pipe(
      imagemin([imagemin.mozjpeg()])
    ).pipe(
      gulp.dest('../dist/img/')
    ).pipe(
      browserSync.stream()
    );
  cb();
}

function sassInit(cb) {
  gulp.src('./scss/styles.scss')
    .pipe(
      sass({
        errLogToConsole: true,
        outputStyle: 'compressed'
      })
    ).pipe(
      rename({
        suffix: '.min'
      })
    ).pipe(
      gulp.dest('../dist/css')
    ).pipe(
      browserSync.stream()
    );
  cb();
}

function jsInit(cb) {
  gulp.src(['./js/*.js', '!./js/index.js'])
    .pipe(
      gulp.src('./js/index.js')
    ).pipe(
      concat('app.js')
    ).pipe(
      babel({
        presets: ['@babel/preset-env']
      })
    ).pipe(
      minify({
        ext: {
          min: '.js'
        },
        noSource: true
      })
    ).pipe(
      gulp.dest('../dist/js/')
    ).pipe(
      browserSync.stream()
    );
  cb();
}

function server(cb) {
  browserSync.create();
  browserSync.init({
    server: {
      baseDir: '../dist'
    },
    port: 2020,
  });
  cb();
}

function globalWatch(cb) {
  gulp.watch('./index.html', htmlInit);
  gulp.watch('./img/*.jpg', imagesInit);
  gulp.watch('./scss/*.scss', sassInit);
  gulp.watch('./js/*.js', jsInit);
  cb();
}

exports.build = gulp.parallel(htmlInit, imagesInit, sassInit, jsInit);
exports.serve = gulp.series(server, globalWatch);