const drawResult = (roundResult, playerChoice, computerChoice) => {
  let round = document.createElement('p');
  round.setAttribute('class', 'game-interface__log-item');
  round.textContent =
    `Round ${currentRound}, ${playerChoice} VS ${computerChoice},`;
  switch (roundResult) {
    case 1:
      round.textContent += ' You\'ve WON';
      break;
    case 0:
      round.textContent += ' DRAW';
      break;
    case -1:
      round.textContent += ' You\'ve LOST';
      break;
    default:
      console.error('Unknown round result!');
  }
  gameLog.appendChild(round);
};