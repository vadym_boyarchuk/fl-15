const init = () => {
  for (let option of document.querySelectorAll('.game-interface__option')) {
    option.addEventListener('click', () => {
      gameLog.setAttribute('style', 'display: block');
      game(option.value.toLowerCase());
    });
  }
  document.querySelector('.game-interface__reset').addEventListener('click', reset);
};