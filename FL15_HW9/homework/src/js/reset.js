const reset = () => {
  gameLog.innerHTML = '';
  currentRound = 1;
  playerWins = 0;
  computerWins = 0;
  gameLog.setAttribute('style', 'display: none');
};