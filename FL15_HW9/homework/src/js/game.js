const game = (playerChoice) => {
  if (currentRound !== 4) {
    let computerChoice = ['rock', 'paper', 'scissors'][Math.round(Math.random() * 2)],
      roundResult;
    switch (playerChoice) {
      case 'rock':
        switch (computerChoice) {
          case 'rock':
            roundResult = 0;
            break;
          case 'paper':
            roundResult = -1;
            ++computerWins;
            break;
          case 'scissors':
            roundResult = 1;
            ++playerWins;
            break;
        }
        break;
      case 'paper':
        switch (computerChoice) {
          case 'rock':
            roundResult = 1;
            ++playerWins;
            break;
          case 'paper':
            roundResult = 0;
            break;
          case 'scissors':
            roundResult = -1;
            ++computerWins;
            break;
        }
        break;
      case 'scissors':
        switch (computerChoice) {
          case 'rock':
            roundResult = -1;
            ++computerWins;
            break;
          case 'paper':
            roundResult = 1;
            ++playerWins;
            break;
          case 'scissors':
            roundResult = 0;
            break;
        }
        break;
      default:
        console.error('Unknown input!');
    }
    drawResult(roundResult, playerChoice, computerChoice);
    ++currentRound;
    if (currentRound === 4) {
      gameResult();
    }
  }
};