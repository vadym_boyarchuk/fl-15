const gameResult = () => {
  let winner = document.createElement('h3');
  winner.setAttribute('class', 'winner-log');
  if (playerWins > computerWins) {
    winner.textContent = 'Player WINS!';
  } else if(computerWins > playerWins) {
    winner.textContent = 'Computer WINS!';
  } else {
    winner.textContent = 'DRAW nobody wins!';
  }
  gameLog.appendChild(winner);
};