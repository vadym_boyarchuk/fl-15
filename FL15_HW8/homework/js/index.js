
$(document).ready(function () {
(function ($) {
  $.fn.todolist = function () {
    const $list = $(".list");
    const $input = $("#add-input");
    const $add = $("#add-submit");
    const $listSearch = $("#list-search");
    const $search = $("#search")
    const $searchInput=$("#search-input")
    const todos = [
      {
        text: "Buy milk",
        done: false
      },
      {
        text: "Play with dog",
        done: true
      }
    ];

    function appendTaskToList(val) {
      $list.append("<li class='item'>" + val + "  <button href='#' class='done-btn'>Done</button> <button href='#' class='cancel-btn'>Cancel Task</button></li>");
    }
    function appendTaskToListSearch(val) {
      $listSearch.append("<li class='item'>" + val + "  <button href='#' class='done-btn'>Done</button> <button href='#' class='cancel-btn'>Cancel Task</button></li>");
    }

    if (localStorage['tasks']) {
      var tasks = JSON.parse(localStorage['tasks']);
    } else {
      var tasks = [];
    }

    for (var i = 0; i < tasks.length; i++) {
      appendTaskToList(tasks[i]);
    }

    var addTask = function () {
      var val = $input.val();
      tasks.push(val);
      localStorage["tasks"] = JSON.stringify(tasks);
      appendTaskToList(val);
      $input.val("").focus();
    }

    $add.on('click', addTask);
    $input.keyup(function (e) {
      if (e.keyCode === 13) {
        addTask();
      }
    });
    $search.on('click', function () {
      var arr = JSON.parse(localStorage['tasks']);
      var val = $searchInput.val();
      var searhcEl = [];
      $.each(arr, function (i, value) {
        searhcEl.push(value);
      })
      var filtered = searhcEl.filter((v) => {
        if (v === val) {
          return v;
        }
      });

      addSearch(filtered);
    })
    var addSearch = function (arr) {
      $.each(arr, function (i, val) {
        appendTaskToListSearch(val)
      })
    }
    $('.done-btn').on('click', function () {
      $(this).parent('li').addClass('done');
    });

    $('.cancel-btn').on('click', function () {
      $(this).parent('li').remove();
    });
  }
}(jQuery));

$(document).todolist();
})